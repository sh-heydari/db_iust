<?php

session_start();

$User_Username =$_SESSION['Username'];
$Search_Username =$_GET['id'];

//check for follow
$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['$and' => [['Follower' => $User_Username],['Followed' =>$Search_Username]]];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.Follow", $query);
$new_array = $rows->toArray();

$follow_state ='Follow me';
if(count($new_array) > 0)
{
    $follow_state = 'Following';
}

//------------------------------------------------------
$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Username' => $Search_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.User", $query);
$new_array = $rows->toArray();

$name = ' '.$new_array[0]->Fname.' '. $new_array[0]->Lname;
$Fname = $new_array[0]->Fname;
$Lname = $new_array[0]->Lname;
$Email = ' '.$new_array[0]->Email.' ';
$University = ' '.$new_array[0]->University;
$Job = ' '.$new_array[0]->Job;
$Skills = ' '.$new_array[0]->Skills;
$Image_src = $new_array[0]->Image_src;

//--------------------------
// Following count
$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Follower' => $Search_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.Follow", $query);
$new_array = $rows->toArray();

$Following_count = Count($new_array);

// Followers count
$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Followed' => $Search_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.Follow", $query);
$new_array = $rows->toArray();

$Followers_count = Count($new_array);

// Posts count
$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Username' => $Search_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.Post", $query);
$new_array = $rows->toArray();

$Post_count = Count($new_array);

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>search</title>

    <!-- Bootstrap core CSS -->

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/blog-home.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script
            src="http://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
</head>

<body>
<form method="post" >

<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">


        <a class="navbar-brand col-sm-2" href="Home.php" >Linkedin IUST</a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="col-sm-3 col-sm-offset-2">
            <input type="text" class="form-control" name="Search_txt" placeholder="Search for..."><span><button name="Search_btn" type="submit" class="btn btn-info glyphicon glyphicon-search"> search</button></span>
        </div>

        <div class="collapse navbar-collapse pull-right" id="navbarResponsive">
            <ul class="navbar-nav ml-auto pull-right" >

                <li class="nav-item active">

                    <a class="nav-link" href="Home.php"><span class="glyphicon glyphicon-home"></span>Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="MyPage.php"><span class="glyphicon glyphicon-user"></span>ME</a> -->
                    <!-- IT should goes to  post page belongs to user-->
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Login.php"><span class="glyphicon glyphicon-log-out"></span> Logout
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

            </ul>
        </div>

    </nav>

<!-- Page Content -->
<div class="container">

    <div class="col-sm-2">

        <img width="250" height="300" src=<?php echo $Image_src ?> >
        <button style="margin-top: 20px" type="submit" class="btn btn-primary" id ="btn_id"  name="Profile_Follow_btn" value=<?Php echo $follow_state ?> ><?Php echo $follow_state ?></button>

    </div>



    <!-- end of row for image and image info  -->
    <div style="margin-left: 120px" class="card col-sm-3 col-sm-push-1" >
        </br>
        <label for="user_name">First Name: </label>
        <input type="text" name ="Mypage_Fname_txt" id ="Mypage_Fname_txt" value="<?php echo $Fname ?>" disabled> </br>



        <label for="user_name">Last Name: </label>
        <input type="text" name ="Mypage_Lname_txt" id ="Mypage_Lname_txt" value="<?php echo $Lname ?>" disabled></br>



        <label for="Email">Email: </label>
        <input type="text" name ="Mypage_Email_txt" id ="Mypage_Email_txt" value="<?php echo $Email ?>" disabled="disabled"></br>



        <label for="university">University: </label>
        <input type="text" name ="Mypage_University_txt" id ="Mypage_University_txt"  value="<?php echo $University ?>" disabled="disabled"></br>



        <label for="job">job: </label>
        <input type="text" name ="Mypage_Job_txt" id ="Mypage_Job_txt" value="<?php echo $Job ?>" disabled="disabled"></br>



        <label for="skills">skills: </label>
        <input type="text" style="margin-bottom: 20px" name ="Mypage_Skills_txt" id ="Mypage_Skills_txt" value="<?php echo $Skills ?>" disabled="disabled">

    </div>

    <div style="margin-left: 230px" class="card col-sm-2" >
        <p class="well" style="margin-top: 10px"> Posts : <span id = "Posts_id"><?php echo $Post_count ?></p></span>
        <p class="well" > Followers : <span id = "Followers_id"><?php echo $Followers_count ?></p></span>
        <p class="well"> Following : <span id = "Following_id"><?php echo $Following_count ?></p></span>
    </div>

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="main.js"></script>


</form>

</body>

</html>




<?php


If(isset($_POST['Profile_Follow_btn']))
{

    if($follow_state == 'Following')
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->delete(['Follower' => $User_Username], ['Followed' => $Search_Username]);
        $result = $Connection->executeBulkWrite('Linkedin.Follow', $bulk);
        $follow_state = 'Follow me';
        echo ('<script> $("#btn_id").html(\'Follow me\'); </script>');
        echo ('<script> $("#Followers_id").html(parseInt($("#Followers_id").html())- 1); </script>');



    }
    else if($follow_state == 'Follow me')
    {
        $bulkWriteManager = new MongoDB\Driver\BulkWrite;
        $insert = ['Follower' => $User_Username, 'Followed' => $Search_Username];
        $bulkWriteManager -> insert($insert); // Inserting Document
        $Connection->executeBulkWrite('Linkedin.Follow', $bulkWriteManager);
        $follow_state = 'Following';
        echo ('<script> $("#btn_id").html(\'Following\'); </script>');
        echo ('<script> $("#Followers_id").html(parseInt($("#Followers_id").html())+ 1); </script>');

    }

}


If(isset($_POST['Search_btn']))
{
    //echo ('<script> $("#Search_txt").prop( "disabled", false ); </script>');
    echo '<script>window.location.href = "Search.php?key='.$_POST['Search_txt'].'"'.';</script>';
}

If(isset($_POST['Search_btn']))
{
    echo '<script>window.location.href = "Search.php?key='.$_POST['Search_txt'].'"'.';</script>';
}

?>

