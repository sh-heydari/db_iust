<?php
session_start();
$User_Username = $_SESSION['Username'];

$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Username' => $User_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.User", $query);
$new_array = $rows->toArray();

$Fname = $new_array[0]->Fname;
$Lname = $new_array[0]->Lname;
$Email = $new_array[0]->Email.' ';
$University = $new_array[0]->University;
$Job = $new_array[0]->Job;
$Skills = $new_array[0]->Skills;
$Image_src = $new_array[0]->Image_src;


?>

<!DOCTYPE html>
<html lang="en">



<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Linkedin IUST</title>

    <!-- Bootstrap core CSS -->
    <!--    <link rel="stylesheet" href="css/sexyForms.css"> -->

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/blog-home.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script
        src="http://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>

</head>

<body>
<form method="post">

<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">


        <a class="navbar-brand col-sm-2" href="Home.php" >Linkedin IUST</a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="col-sm-3 col-sm-offset-2">
            <input type="text" class="form-control" name="Search_txt" placeholder="Search for..."><span><button name="Search_btn" type="submit" class="btn btn-info glyphicon glyphicon-search"> search</button></span>
        </div>

        <div class="collapse navbar-collapse pull-right" id="navbarResponsive">
            <ul class="navbar-nav ml-auto pull-right" >

                <li class="nav-item active">

                    <a class="nav-link" href="Home.php"><span class="glyphicon glyphicon-home"></span>Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="MyPage.php"><span class="glyphicon glyphicon-user"></span>ME</a> -->
                    <!-- IT should goes to  post page belongs to user-->
                </li>

            </ul>
        </div>

    </nav>

    <input type="hidden" name="Edit_State" id="Edit_State" value="0" />
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="card col-sm-3">
            <h5 class="card-header" >user information</h5>
            <!-- user information like unioversity or career goes here -->
            <div class="card-body">
                <div class="row" id="user_image">
                    <img width="260" height="260" src=<?php echo $Image_src ?> >
                </div>
                </br>

                <div>
                    <button type="submit"  name ="Edit_btn_name" id ="Edit_btn_id" class="btn btn-default btn-block" >Edit Profile</button>

                </div>
            </div>
        </div>



        <!-- end of row for image and image info  -->
        <div class="card col-sm-3 col-sm-push-1" >



                <label for="user_name">First Name: </label>
                <input type="text" name ="Mypage_Fname_txt" id ="Mypage_Fname_txt" value="<?php echo $Fname ?>" disabled>



                <label for="user_name">Last Name: </label>
                <input type="text" name ="Mypage_Lname_txt" id ="Mypage_Lname_txt" value="<?php echo $Lname ?>" disabled>



                <label for="Email">Email: </label>
                <input type="text" name ="Mypage_Email_txt" id ="Mypage_Email_txt" value="<?php echo $Email ?>" disabled="disabled">



                <label for="university">University: </label>
                <input type="text" name ="Mypage_University_txt" id ="Mypage_University_txt"  value="<?php echo $University ?>" disabled="disabled">



                <label for="job">job: </label>
                <input type="text" name ="Mypage_Job_txt" id ="Mypage_Job_txt" value="<?php echo $Job ?>" disabled="disabled">



                <label for="skills">skills: </label>
                <input type="text" name ="Mypage_Skills_txt" id ="Mypage_Skills_txt" value="<?php echo $Skills ?>" disabled="disabled">


                <label for="password">old password: </label>
                <input type="text" name ="Mypage_Old_Password_txt" id ="Mypage_Old_Password_txt" disabled="disabled">
                <label for="new_pass">new password: </label>
                <input type="text" name ="Mypage_new1_Password_txt" id ="Mypage_new1_Password_txt" id="new_pass" disabled="disabled">
                <label for="re_pass">retype password: </label>
                <input type="text" name ="Mypage_new2_Password_txt" id ="Mypage_new2_Password_txt" id="re_pass" disabled="disabled">




        </div>
        <!-- end of new post -->



    </div>
    <!-- end of row 1 -->
    <div style="margin-top: 30px" class="row">
    </div>
    <!-- end of row 2 -->


</div>
<!-- end of container -->



</form>
</body>

</html>


<script>
    $("#Edit_btn_id").click(function (e) {
        if( $("#Edit_State").val() != '1' )
        {
            e.preventDefault();
            $("#Edit_btn_id").html("Save");
            $("#Edit_State").val('1');

            $("#Mypage_Fname_txt").prop( "disabled", false );
            $("#Mypage_Lname_txt").prop( "disabled", false );
            $("#Mypage_Email_txt").prop( "disabled", false );
            $("#Mypage_University_txt").prop( "disabled", false );
            $("#Mypage_Job_txt").prop( "disabled", false );
            $("#Mypage_Skills_txt").prop( "disabled", false );
            $("#Mypage_Old_Password_txt").prop( "disabled", false );
            $("#Mypage_new1_Password_txt").prop( "disabled", false );
            $("#Mypage_new2_Password_txt").prop( "disabled", false );
        }
    })

</script>

<?php

If(isset($_POST['Search_btn']))
{
    echo '<script>window.location.href = "Search.php?key='.$_POST['Search_txt'].'"'.';</script>';
}
else {
    if (!empty($_POST)) {

        $Fname = $_POST['Mypage_Fname_txt'];
        $Lname = $_POST['Mypage_Lname_txt'];
        $Email = $_POST['Mypage_Email_txt'];
        $University = $_POST['Mypage_University_txt'];
        $Job = $_POST['Mypage_Job_txt'];
        $Skills = $_POST['Mypage_Skills_txt'];

        // check password old
        $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        $filter = ['Username' => $User_Username];
        $query = new MongoDB\Driver\Query($filter);
        $rows = $Connection->executeQuery("Linkedin.User", $query);
        $new_array = $rows->toArray();

        if ($new_array[0]->Password == $_POST['Mypage_Old_Password_txt']) {
            //check pass1 with pass2
            if ($_POST['Mypage_new1_Password_txt'] == $_POST['Mypage_new2_Password_txt']) {
                $bulk = new MongoDB\Driver\BulkWrite;
                $bulk->update(
                    ['Username' => $User_Username],
                    ['$set' => ['Fname' => $Fname, 'Lname' => $Lname, 'Email' => $Email, 'University' => $University, 'Job' => $Job, 'Skills' => $Skills, 'Password' => $_POST['Mypage_new2_Password_txt']]]
                );
                $result = $Connection->executeBulkWrite('Linkedin.User', $bulk);
                echo '<script>window.location.href = "MyPage.php";</script>';
            } else // pass incorrect
            {
                $message = "New Passwpord is incorrect";
                echo '<script type="text/javascript">alert("' . $message . '")</script>';
            }
        } elseif ($_POST['Mypage_Old_Password_txt'] == "") {
            $bulk = new MongoDB\Driver\BulkWrite;
            $bulk->update(
                ['Username' => $User_Username],
                ['$set' => ['Fname' => $Fname, 'Lname' => $Lname, 'Email' => $Email, 'University' => $University, 'Job' => $Job, 'Skills' => $Skills]]
            );
            $result = $Connection->executeBulkWrite('Linkedin.User', $bulk);
            echo '<script>window.location.href = "MyPage.php";</script>';

        } else {
            $message = "Old Passwpord is incorrect";
            echo '<script type="text/javascript">alert("' . $message . '")</script>';
        }

    }


}

// POST LOad
$filter = ['Username' => $User_Username];
$options = array(
    "sort" => array("Date" => -1,"Time" => -1),
);

$query = new MongoDB\Driver\Query($filter,$options);
$rows = $Connection->executeQuery("Linkedin.Post", $query);
$new_array = $rows->toArray();



for ($x = 0 ; $x < count($new_array) ; $x++)
{
    //like state
    $array = $new_array[$x]->Like;
    $Like_Number = "0";
    $Like_State = "Like";

    if($array != NULL)
    {
        $Like_Number = count($array);

        $filter = ['Username' => ['$in' => $array ]];
        $query = new MongoDB\Driver\Query($filter);
        $rows = $Connection->executeQuery("Linkedin.User", $query);
        $new = $rows->toArray();

        if  (count($new) >0)
        {
            $Like_State = "Liked";
        }
    }



    //---------------------------


    $Image_src = $new_array[$x]->Image_src;
    $Text = $new_array[$x]->Text;
    $Date = $new_array[$x]->Date . '  ' . $new_array[$x]->Time;
    $Post_Like_Number_id = 'Like_Number_'.$new_array[$x]->_id;
    $Post_Like_id = 'Like_'.$new_array[$x]->_id;
    $Post_Comment_btn_id = 'Comment_btn_'.$new_array[$x]->_id;
    $Post_Comment_txt_id = 'Comment_txt_'.$new_array[$x]->_id;

    echo ('<form method="post" enctype= multipart/form-data ><div class="row">

        <div class="card mb-7 col-sm-offset-3" id="posts_place">
            <h2>'.$User_Username.'</h2>    
            <img width="650" height="350"  src='.$Image_src.' alt="Card image cap">
            <div class="card-body">
                <p class="card-text">'.$Text.'</p>
                <div class="pull-right">
                    
                        <span id='.$Post_Like_id.' onclick="post_like_unlike(this)" class="btn btn-info btn-lg glyphicon glyphicon-thumbs-up">'.$Like_State.'</span> 
                    
                    <span id = '.$Post_Like_Number_id.'>'.$Like_Number.'</span>
                </div>
                <div class="row card">

                    <div class="form-group">
                            <textarea class="form-control" id='.$Post_Comment_txt_id.' rows="2"></textarea>
                            <button type="button" id='.$Post_Comment_btn_id.' onclick="post_comment(this)" class="glyphicon glyphicon-circle-arrow-up">send</button>
                    </div>

                </div>
            </div>
            <div class="card-footer text-muted">Posted on '.$Date.' </div>
        </div>
    </div></form>');
}


?>

<script type="text/javascript">


    function post_like_unlike(post_id_like_unlike)
    {

        var Post_like_id = post_id_like_unlike.id;
        var Post_id = ((post_id_like_unlike.id).split("_"))[1];
        var Post_like_number_id = "Like_Number_"+Post_id;

        if($("#"+Post_like_id).html() == "Like")
        {
            $("#"+Post_like_id).html('Liked');
            var temp = $("#"+Post_like_number_id).html();
            $("#"+Post_like_number_id).html(parseInt(temp)+1);

            // like ra save kon
            var request = $.ajax({ url: "Like_ajax.php", type: "POST", data: {id : Post_id , State : "Like" , User_Username :'<?php echo $User_Username ?>' }, dataType: "html" });
            request.done(function(msg) { alert("Liked") ; });

        }
        else
        {
            $("#"+Post_like_id).html('Like');
            var temp = $("#"+Post_like_number_id).html();
            $("#"+Post_like_number_id).html(parseInt(temp)-1);

            // like ra delete kon
            var request = $.ajax({ url: "Like_ajax.php", type: "POST", data: {id : Post_id , State : "DisLike" , User_Username :'<?php echo $User_Username ?>'}, dataType: "html" });
            request.done(function(msg) { alert("Disliked") ; });

        }
    }

    function post_comment(post_comment_btn)
    {
        var Post_comment_btn_id = post_comment_btn.id;
        var Post_id = ((post_comment_btn.id).split("_"))[2];
        var Post_comment_txt_id = "Comment_txt_"+Post_id;

        if ($("#"+Post_comment_txt_id).val() != "");
        {
            var request = $.ajax({ url: "Comment_Ajax.php", type: "POST", data: {id : Post_id , Text : $("#"+Post_comment_txt_id).val() , User_Username :'<?php echo $User_Username ?>'}, dataType: "html" });
            request.done(function(msg) { alert("Comment Sended") ; });
            $("#"+Post_comment_txt_id).val("");
        }



    }
</script>


<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; IUST 2018</p>
    </div>
    <!-- /.container -->
</footer>