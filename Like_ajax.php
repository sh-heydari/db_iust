<?php

$Post_id = $_POST['id'];
$Post_State = $_POST['State'];
$User_Username = $_POST['User_Username'];

if($Post_State == 'Like')
{
    $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $filter = ['_id' => new MongoDB\BSON\ObjectID("$Post_id")];
    $query = new MongoDB\Driver\Query($filter);
    $rows = $Connection->executeQuery("Linkedin.Post", $query);
    $new_array = $rows->toArray();
    $array = $new_array[0]->Like;


    if($array != null and count($array) > 0)
    {
        $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(
            ['_id' => new MongoDB\BSON\ObjectID("$Post_id")],
            ['$push' => ['Like' => $User_Username]]
        );
        $result = $Connection->executeBulkWrite('Linkedin.Post', $bulk);
    }
    else
    {
        $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(
            ['_id' => new MongoDB\BSON\ObjectID("$Post_id")],
            ['$set' => ['Like' => array($User_Username)]]
        );
        $result = $Connection->executeBulkWrite('Linkedin.Post', $bulk);
    }


}

else if($Post_State == "DisLike")
{
    $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->update(
        ['_id' => new MongoDB\BSON\ObjectID("$Post_id")],
        ['$pull' => ['Like' => $User_Username]]
    );
    $result = $Connection->executeBulkWrite('Linkedin.Post', $bulk);
}
else if($Post_State == "Delete")
{
    $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->delete(['_id' => new MongoDB\BSON\ObjectID("$Post_id") ]);
    $Connection->executeBulkWrite('Linkedin.Post', $bulk);

    $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->delete(['Post_id' => $Post_id ]);
    $Connection->executeBulkWrite('Linkedin.Comment', $bulk);

}