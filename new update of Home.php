<?php
session_start();
$User_Username =$_SESSION['Username'];


$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Username' => $User_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.User", $query);
$new_array = $rows->toArray();

$Image_src = $new_array[0]->Image_src;

?>






<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Linkedin IUST</title>

    <!-- Bootstrap core CSS -->
    <!--    <link rel="stylesheet" href="css/sexyForms.css"> -->

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/blog-home.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>

<body bgcolor="red" >
<form method="post" enctype= multipart/form-data>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">


    <a class="navbar-brand col-sm-2" href="Home.php" >Linkedin IUST</a>
    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
    <div class="col-sm-3 col-sm-offset-2">
        <input type="text" class="form-control" name="Search_txt" placeholder="Search for..."><span><button name="Search_btn" type="submit" class="btn btn-info glyphicon glyphicon-search"> search</button></span>
    </div>

    <div class="collapse navbar-collapse pull-right" id="navbarResponsive">
        <ul class="navbar-nav ml-auto pull-right" >

            <li class="nav-item active">

                <a class="nav-link" href="Home.php"><span class="glyphicon glyphicon-home"></span> Home
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="MyPage.php"><span class="glyphicon glyphicon-user"></span> ME</a> -->
                <!-- IT should goes to  post page belongs to user-->
            </li>
            <li class="nav-item">
            <a class="nav-link" href="Login.php"><span class="glyphicon glyphicon-log-out"></span> Logout
                <span class="sr-only">(current)</span>
            </a>
            </li>

        </ul>
    </div>

</nav>


<!-- Page Content -->
<div class="container">
    <h1 class="my-4" >welcome
        <small>Dear User</small>
    </h1>
    <div class="row">
        <div class="card col-sm-3">
            <h5 class="card-header" >user information</h5>
            <!-- user information like unioversity or career goes here -->
            <div class="card-body">
                <div class="row" id="user_image">
                    <!-- for image  -->
                    <img  width="205" height="150" src=<?php echo $Image_src ?> alt=<?php echo $User_Username ?>>
                </div>
                <div class="row" id="user_info">
                </div>
            </div>

        </div>
        <!-- end of row for image and image info  -->
        <div class="card col-sm-8 col-sm-push-1" >

            <form >
                <div class="form-group">

                    <label for="exampleFormControlTextarea1">Create New Post</label>
                    <textarea name="Home_Post_Text" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div><input name="Home_Post_Image" type="file"></div>
                <div><button type="submit"  name="btn_name" class="btn btn-success pull-right glyphicon glyphicon-circle-arrow-up"> send</button></div>

            </form>
        </div>
        <!-- end of new post -->



    </div>
    <!-- end of row 1 -->

    <!-- end of row 2 -->


</div>
<!-- end of container -->








<?php
If(isset($_POST['Search_btn']))
{
    //echo ('<script> $("#Search_txt").prop( "disabled", false ); </script>');
    echo '<script>window.location.href = "Search.php?key='.$_POST['Search_txt'].'"'.';</script>';
}
if (isset($_POST['btn_name']))
{
    date_default_timezone_set("Iran");
    // Save image
    $tmp = $_FILES["Home_Post_Image"]['tmp_name'];
    $image_path = "../Linkedin/Post_image/".$User_Username.'_'.mt_rand(1,100000000000).".jpg";
    move_uploaded_file($tmp,$image_path);

    // insert in User Collection
    $bulkWriteManager = new MongoDB\Driver\BulkWrite;
    $insert = ['Username' => $User_Username
        , 'Text' => $_POST['Home_Post_Text']
        , 'Image_src' => $image_path
        , 'Like' => ''
        , 'Comment' => ''
        , 'Date' =>date("Y/m/d")
        , 'Time' => date("H:i:s")
        ];

    $bulkWriteManager -> insert($insert); // Inserting Document
    $Connection->executeBulkWrite('Linkedin.Post', $bulkWriteManager);

}

//-----------------------------------------------------------------------------------
//------------------------Post-------------------------------------------------------

// Load Following list
$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Follower' => $User_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.Follow", $query);

$n_array = $rows->toArray();


$Followed_list = array($User_Username);
for($i = 1 ; $i <= count($n_array) ; $i++)
{
    array_push($Followed_list,$n_array[$i-1]->Followed);
}


//Comment Func
function Comment_func ($P_id)
{
    $Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $filter = ['Post_id' => "$P_id"];
    $options = array(
        "sort" => array("Date" => -1,"Time" => -1),
    );

    $query = new MongoDB\Driver\Query($filter,$options);
    $rows = $Connection->executeQuery("Linkedin.Comment", $query);
    $new = $rows->toArray();

    $text = "";
    for ($i =0 ; $i <count($new) ; $i++)
    {
        $temp = $new[$i]->User_Username . ' : ' . $new[$i]->Text ;
        $text = $text.'<p class="card-text">'.$temp.'</p>';
    }
    return $text;
}



// POST LOad
$filter = ['Username' => ['$in' => $Followed_list ]];
$options = array(
    "sort" => array("Date" => -1,"Time" => -1),
);

$query = new MongoDB\Driver\Query($filter,$options);
$rows = $Connection->executeQuery("Linkedin.Post", $query);
$new_array = $rows->toArray();



for ($x = 0 ; $x < count($new_array) ; $x++)
{
//like state
    $array = $new_array[$x]->Like;
    $Like_Number = "0";
    $Like_State = "Like";

    if($array != NULL)
    {
        $Like_Number = count($array);

        $filter = ['Username' => ['$in' => $array ]];
        $query = new MongoDB\Driver\Query($filter);
        $rows = $Connection->executeQuery("Linkedin.User", $query);
        $new = $rows->toArray();

        if  (count($new) >0)
        {
            $Like_State = "Liked";
        }
    }



//---------------------------


    $Image_src = $new_array[$x]->Image_src;
    $Text = $new_array[$x]->Text;
    $Date = $new_array[$x]->Date . '  ' . $new_array[$x]->Time;
    $Post_Like_Number_id = 'Like_Number_'.$new_array[$x]->_id;
    $Post_Like_id = 'Like_'.$new_array[$x]->_id;
    $Post_Comment_btn_id = 'Comment_btn_'.$new_array[$x]->_id;
    $Post_Comment_txt_id = 'Comment_txt_'.$new_array[$x]->_id;

    echo ('<form method="post" enctype= multipart/form-data ><div STYLE="margin-top: 40px" class="row">

        <div class="card mb-7 col-sm-offset-3" id="posts_place">
            <h2>'.$User_Username.'</h2>
            <img width="650" height="350"  src='.$Image_src.' alt="Card image cap">
            <div class="card-body">
                <p class="card-text">'.$Text.'</p>
                
                
                <div class="card-header">
                     '.Comment_func($new_array[$x]->_id).'
                </div>
                
                </br>
                
                <div class="pull-right">

                    <span id='.$Post_Like_id.' onclick="post_like_unlike(this)" class="btn btn-info btn-lg glyphicon glyphicon-thumbs-up">'.$Like_State.'</span>

                    <span id = '.$Post_Like_Number_id.'>'.$Like_Number.'</span>
                </div>
                <div class="row card">

                    <div class="form-group">
                        <textarea class="form-control" id='.$Post_Comment_txt_id.' rows="2"></textarea>
                        <button type="submit" id='.$Post_Comment_btn_id.' onclick="post_comment(this)" class="glyphicon glyphicon-circle-arrow-up">send</button>
                    </div>

                </div>
            </div>
            <div class="card-footer text-muted">Posted on '.$Date.' </div>
        </div>
    </div></form>');
}


?>

<script type="text/javascript">


    function post_like_unlike(post_id_like_unlike)
    {

        var Post_like_id = post_id_like_unlike.id;
        var Post_id = ((post_id_like_unlike.id).split("_"))[1];
        var Post_like_number_id = "Like_Number_"+Post_id;

        if($("#"+Post_like_id).html() == "Like")
        {
            $("#"+Post_like_id).html('Liked');
            var temp = $("#"+Post_like_number_id).html();
            $("#"+Post_like_number_id).html(parseInt(temp)+1);

            // like ra save kon
            var request = $.ajax({ url: "Like_ajax.php", type: "POST", data: {id : Post_id , State : "Like" , User_Username :'<?php echo $User_Username ?>' }, dataType: "html" });
            request.done(function(msg) { alert("Liked") ; });

        }
        else
        {
            $("#"+Post_like_id).html('Like');
            var temp = $("#"+Post_like_number_id).html();
            $("#"+Post_like_number_id).html(parseInt(temp)-1);

            // like ra delete kon
            var request = $.ajax({ url: "Like_ajax.php", type: "POST", data: {id : Post_id , State : "DisLike" , User_Username :'<?php echo $User_Username ?>'}, dataType: "html" });
            request.done(function(msg) { alert("Disliked") ; });

        }
    }

    function post_comment(post_comment_btn)
    {
        var Post_comment_btn_id = post_comment_btn.id;
        var Post_id = ((post_comment_btn.id).split("_"))[2];
        var Post_comment_txt_id = "Comment_txt_"+Post_id;

        if ($("#"+Post_comment_txt_id).val() != "");
        {
            var request = $.ajax({ url: "Comment_Ajax.php", type: "POST", data: {id : Post_id , Text : $("#"+Post_comment_txt_id).val() , User_Username :'<?php echo $User_Username ?>'}, dataType: "html" });
            request.done(function(msg) { alert("Comment Sended") ; });
            $("#"+Post_comment_txt_id).val("");
        }



    }
</script>


    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; IUST 2018</p>
        </div>
        <!-- /.container -->
    </footer>

</form>
</body>

</html>