<?php

$Post_id = $_POST['id'];
$text = $_POST['Text'];
$User_Username = $_POST['User_Username'];

date_default_timezone_set("Iran");


$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$bulkWriteManager = new MongoDB\Driver\BulkWrite;

$insert = ['Post_id' => $Post_id
    , 'User_Username' => $User_Username
    , 'Text' => $text
    , 'Date' =>date("Y/m/d")
    , 'Time' => date("H:i:s")];

$bulkWriteManager -> insert($insert); // Inserting Document
$Connection->executeBulkWrite('Linkedin.Comment', $bulkWriteManager);

?>