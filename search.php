
<?php
session_start();

$search_key =$_GET['key'];
$User_Username =$_SESSION['Username'];

$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['Username' => $User_Username];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.User", $query);
$new_array = $rows->toArray();

$User_Job = $new_array[0]->Job;
$User_University = $new_array[0]->University;

echo ('
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>search</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/sexyForms.css">

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/blog-home.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
<form method="post">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">


        <a class="navbar-brand col-sm-2" href="Home.php" >Linkedin IUST</a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="col-sm-3 col-sm-offset-2">
            <input type="text" class="form-control" name="Search_txt" placeholder="Search for..."><span><button name="Search_btn" type="submit" class="btn btn-info glyphicon glyphicon-search"> search</button></span>
        </div>

        <div class="collapse navbar-collapse pull-right" id="navbarResponsive">
            <ul class="navbar-nav ml-auto pull-right" >

                <li class="nav-item active">

                    <a class="nav-link" href="Home.php"><span class="glyphicon glyphicon-home"></span>Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="MyPage.php"><span class="glyphicon glyphicon-user"></span>ME</a> -->
                    <!-- IT should goes to  post page belongs to user-->
                </li>

            </ul>
        </div>

    </nav>

<div class="container">
<div class="col-md-6">
<h1>Result list</h1>
');

$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$regex = new MongoDB\BSON\Regex ($search_key);
$filter = ['$or' => [['Fname' => $regex],['Lname' =>$regex]]];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.User", $query);
$new_array = $rows->toArray();

for ($x = 0; $x < count($new_array); $x++)
{
    if($new_array[$x]->Username != $User_Username ) {
        echo('<div class="row searchstyle" >
        <div class="col-md-6" >
        <a href="Profile.php?id=');
        echo $new_array[$x]->Username;
        echo('" > <img name =' . $new_array[$x]->Username . ' width="150"  height="150" id = ' . $new_array[$x]->Username . '  src = ');
        echo $new_array[$x]->Image_src;
        echo(' alt = "steve" > </a> <div name = "search_recommend_5_lable" id = "rec_name" >');
        echo $new_array[$x]->Fname . '  ' . $new_array[$x]->Lname . '<br>';
        echo 'University : ' . $new_array[$x]->University . '<br>' . 'Job : ' . $new_array[$x]->Job . '<br><br>';
        echo('</div ></div ></div >');
    }
}
echo '</div>';
echo '<div class="col-md-6"> <h1>recommend list</h1>';

$Connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
$filter = ['$or' => [['University' => $User_University],['Job' =>$User_Job]]];
$query = new MongoDB\Driver\Query($filter);
$rows = $Connection->executeQuery("Linkedin.User", $query);
$new_array = $rows->toArray();

for ($x = 0; $x < count($new_array); $x++)
{
    if($new_array[$x]->Username != $User_Username )
    {
        echo('<div class="row searchstyle" >
        <div class="col-md-6" >
        <a href="Profile.php?id=');
        echo $new_array[$x]->Username;
        echo('" > <img name =' . $new_array[$x]->Username . ' width="150"  height="150" id = ' . $new_array[$x]->Username . ' src = ');
        echo $new_array[$x]->Image_src;
        echo(' alt = "steve" > </a> <div name = "search_recommend_5_lable" id = "rec_name" >');
        echo $new_array[$x]->Fname . '  ' . $new_array[$x]->Lname . '<br>';
        echo 'University : ' . $new_array[$x]->University . '<br>' . 'Job : ' . $new_array[$x]->Job . '<br><br>';
        echo('</div ></div ></div >');
    }
}
echo '</div>';

echo '</div>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="main.js"></script>
    
    </form>
  </body>
</html>
';


If(isset($_POST['Search_btn']))
{
    echo '<script>window.location.href = "Search.php?key='.$_POST['Search_txt'].'"'.';</script>';
}
?>
